package covidalert.controllers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")
public class HomeController {

    @Value("${app.version}")
            private String appVersion;

    @GetMapping
    @RequestMapping("/")
    public Map getAppVersion() {
        HashMap<String, String> info = new HashMap<String, String>();
        info.put("appVersion", appVersion);
        return info;
    }


}
